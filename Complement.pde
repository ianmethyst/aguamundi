/**
 * Esta clase es para los botones que están por fuera del planisferio
 **/

class Complement extends Surface {

  String text;

  Animation button;

  boolean open;

  public Complement(String name, boolean left) {

    size = new PVector(300, 100);

    path = sketchPath() + "/data/complements/" + name;

    // Load JSON

    JSONObject data = null;

    File [] files = new File(path).listFiles(new FilenameFilter() {
      @Override
        public boolean accept(File dir, String name) {
        return name.endsWith(".json");
      }
    }
    );

    for (File f : files) {
      data = loadJSONObject(f);
      break;
    }

    // Error checking

    if (data == null) {
      println("Error: json file not found in " + path);
      return;
    }

    String JSONname = data.getString("name");

    if (!name.equals(JSONname)) {
      println("Warning: name argument passed to Region constructor differs from name found in JSON key");
    }

    // Set variables

    id = data.getInt("id");

    this.name = JSONname;

    text =  data.getString("text");

    surface = ks.createCornerPinSurface(int(size.x), int(size.y), 20);
    offscreen = createGraphics(int(size.x), int(size.y), P3D);

    button = rotatingButton;
  }

  @Override
    public void update() {
    renderOffscreen();
  }

  @Override
    protected void renderOffscreen() {
    offscreen.beginDraw();
    offscreen.clear();
    offscreen.imageMode(CENTER);
    button.render(offscreen, offscreen.width / 2, offscreen.height / 2, offscreen.width / 2, offscreen.height / 2);
    offscreen.endDraw();
  }

  public void handleTouch(int plate) {
    if (id == plate) {
      open = !open;
    }
  }

  @Override
    public void handleKeyPress() {
    if (surface.isMouseOver()) {
      if (key == '3') {
        println(name + ": position saved from " + path);
        savePosition(path);
      }

      if (key == '1') {
        println(name + ": position saved from " + path);
        loadPosition(path);
      }
    }
  }
}
