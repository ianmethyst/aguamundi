import java.io.File;
import java.io.FilenameFilter;

import ianmethyst.animation.Animation;
import ianmethyst.animation.AnimationLoader;

import ianmethyst.keystone.*;
import processing.serial.*; 

AnimationLoader animationLoader;
Keystone ks;

final int lf = 10;    // Linefeed in ASCII

Serial[] arduinos;

// Common animations
Animation rotatingButton;
Animation regionIndicator;

// Arrays
Surface[] surfaces;
Region[] regions;
Menu[] menues;
Complement[] complements;

Region activeRegion;
Menu activeMenu;

PFont font;

boolean calibratingIndividual = false;
int currentSurface = 0;
int totalSurfaces;

void setup() {
  size(1024, 768, P3D);

  animationLoader = new AnimationLoader(this);

  ks = new Keystone(this);

  rotatingButton = new Animation(animationLoader.loadAnimation(sketchPath() + "/data/shared/rotatingButton/"));
  rotatingButton.setInfiniteLoop();
  rotatingButton.play();

  regionIndicator = new Animation(animationLoader.loadAnimation(sketchPath() + "/data/shared/indicator/"));
  regionIndicator.setInfiniteLoop();
  regionIndicator.play();

  regions = new Region[9];
  menues = new Menu[3];
  complements = new Complement[1];

  menues[0] = new Menu(3, "center", 270, 100, 7);
  menues[1] = new Menu(2, "left", 160, 48, 4);
  menues[2] = new Menu(4, "right", 180, 48, 4);

  regions[0] = new Region("antartida", 320, 180);
  regions[1] = new Region("sudamerica", 320, 180);
  regions[2] = new Region("argentina", 320, 180);
  regions[3] = new Region("norteamerica", 320, 180);
  regions[4] = new Region("centroamerica", 320, 180);
  regions[5] = new Region("asia", 320, 180);
  regions[6] = new Region("europa", 320, 180);
  regions[7] = new Region("africa", 320, 180);
  regions[8] = new Region("oceania", 320, 180);

  complements[0] = new Complement("left1", true);

  font = createFont("data/font/Barlow-Regular.otf", 15);

  arduinos = new Serial[3];

  for (int i = 0; i < Serial.list().length; i++) {
    String s = Serial.list()[i];
    if (s.contains("USB") || s.contains("ACM")) {
      for (int j = 0; j < arduinos.length; j++) {
        if (arduinos[j] == null) {
          arduinos[j] = new Serial(this, s, BAUD_RATE);
          arduinos[j].bufferUntil(LF);
          break;
        }
      }
    }
  }

  totalSurfaces = regions.length + menues.length + complements.length;
  surfaces = new Surface[totalSurfaces];

  for (int i = 0; i < totalSurfaces; i++) {  
    if (i < regions.length) {
      surfaces[i] = regions[i];
    } else if (i >= regions.length && i < menues.length + regions.length) {
      surfaces[i] = menues[i - regions.length];
    } else if (i >= regions.length + menues.length && i < menues.length + regions.length + complements.length) {
      surfaces[i] = complements[i - regions.length - menues.length];
    }
  }

  activeRegion = null;
  loadAllPositions();
}

void draw() {
  if (ks.isCalibrating()) {
    background(255, 0, 255);
    if (calibratingIndividual) {
      for (int i = 0; i < totalSurfaces; i++) {
        if (i == currentSurface) {
          surfaces[i].getSurface().setSelected(true);
        } else {
          surfaces[i].getSurface().setSelected(false);
        }
      }
    } else {
      for (Surface s : surfaces) {
        s.getSurface().setSelected(true);
      }
    }
  } else {
    background(0);
  }

  rotatingButton.update();
  regionIndicator.update();

  for (Menu m : menues) {
    m.update();
    m.display();
  }

  for (Complement c : complements) {
    c.update();
    c.display();
  }

  for (Region r : regions) {
    r.update();
    r.display();
  }

  if (ks.isCalibrating()) {
    push();
    fill(0, 255, 255);
    ellipse(mouseX, mouseY, 20, 20); 
    pop();
  }
}

// Events code
void mousePressed() {
  if (!ks.isCalibrating()) {

    if (activeMenu != null) {
      activeMenu.handleMouseTouch();
    }

    if (activeRegion != null) {
      if (activeRegion.getSurface().isMouseOver()) {
        return;
      }
    }

    activeRegion = null;

    for (Region r : regions) {
      r.handleMouseTouch();
    }
  }
}

void loadAllPositions() {
  for (Region r : regions) {
    r.loadPosition(r.getPath());
  }

  for (Menu m : menues) {
    m.loadPosition(m.getPath());
  }
}

void keyPressed() {
  for (Region r : regions) {
    r.handleKeyPress();
  }

  for (Menu m : menues) {
    m.handleKeyPress();
  }

  for (Complement c : complements) {
    c.handleKeyPress();
  }

  switch(key) {
  case 'c':
  case 'C':
    ks.toggleCalibration();
    break;

  case '0':
    loadAllPositions();
    break;

  case '8':
    for (Region r : regions) {
      r.savePosition(r.getPath());
    }

    for (Menu m : menues) {
      m.savePosition(m.getPath());
    }
    break;

  case 'i':
  case 'I':
    calibratingIndividual = !calibratingIndividual;
    break;

  case '+':
  case 's': 
    if (calibratingIndividual) {
      currentSurface++;

      if (currentSurface > totalSurfaces) {
        currentSurface = 0;
      }
    }
    break;

  case '-':
  case 'a':
    if (calibratingIndividual) {
      currentSurface--;

      if (currentSurface < 0) {
        currentSurface = totalSurfaces;
      }
    }
    break;
  }
}

void serialEvent(Serial s) {
  String event = s.readString();

  String[] splitted = event.split("_");

  int id = Integer.parseInt(splitted[0].substring(0, 1));
  int sub = Integer.parseInt(splitted[1].substring(0, 1));
  
  println(id + " - " + sub);

  switch (id) {

  case 1:
    if (activeRegion != null) {
      if (activeRegion.getId() == sub) {
        break;
      }
    }

    activeRegion = null;

    for (Region r : regions) {
      r.handleTouch(sub);
    }

    break;

  case 2:
  case 3:
  case 4: 
    if (activeMenu != null) {
      activeMenu.handleTouch(id, sub);
    }
    break;


  case 5:
  case 6: 
    {
      for (Complement c : complements) {
        c.handleTouch(sub);
      }
      break;
    }
  default: 
    println("ERROR: ID not mapped to anything");
  }
}
