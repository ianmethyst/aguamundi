/**
 * Esta clase sirve para mostrar un menú que se vinculara con diferentes areas para
 * mostrar los botones (Button) de cada uno de sus objetos Item.
 */

class Menu extends Surface {

  private Region currentRegion;

  int buttons;

  public Menu(int id, String name, int w, int h, int b) {
    this.name = name;
    this.id = id;
    size = new PVector(w, h);

    buttons = b;

    path = sketchPath() + "/data/menues/" + name;

    surface = ks.createCornerPinSurface(int(size.x), int(size.y), 20);
    offscreen = createGraphics(int(size.x), int(size.y), P3D);

    offscreen.beginDraw();
    offscreen.textAlign(CENTER, BOTTOM);
    offscreen.endDraw();
  }

  @Override
    public void update() {
    renderOffscreen();
  }

  @Override
    protected void renderOffscreen() {
    offscreen.beginDraw();
    offscreen.background(0, 150);
    if (currentRegion != null) {
      for (Item i : currentRegion.getItems()) {
        i.getButton().render(offscreen);
      }
    }
    offscreen.endDraw();
  }

  public void handleTouch(int group, int plate) {
    {
      if (group == id) {
        for (Item i : currentRegion.getItems()) {
          i.setActive(false);

          if (plate == i.getId()) {
            println(i.getId());
            i.handleTouch();
          }
        }
      }
    }
  }

  public void handleMouseTouch() {
    if (surface.isMouseOver()) {
      PVector mouse = surface.getTransformedMouse();
      int plate = floor(map(mouse.y, 0, offscreen.height, 0, buttons));
      handleTouch(id, plate);
    }
  }

  @Override
    public void handleKeyPress() {
    if (surface.isMouseOver()) {
      if (key == '3') {
        println(name + ": position saved from " + path);
        savePosition(path);
      }

      if (key == '1') {
        println(name + ": position saved from " + path);
        loadPosition(path);
      }
    }
  }

  void setCurrentRegion(Region r) {
    currentRegion = r;
  }

  public int getButtons() {
    return buttons;
  }
}
