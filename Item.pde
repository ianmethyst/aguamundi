class Item {
  private int id;
  private Button button;  
  private Animation animation;
  private boolean active;
  private String animationPath;

  public Item(int id, Menu menu, String buttonLabel, String animationPath) {
    this.id = id;
    button = new Button(buttonLabel, 0, menu.getSize().y * map(id, 0, menu.getButtons(), 0, 1), menu.getSize().x, menu.getSize().y / menu.getButtons());
    this.animationPath = animationPath;
    //animation = new Animation(animationLoader.loadAnimation(animationPath));
  }

  public void handleTouch() {
    setActive(true);
  }

  public int getId() {
    return id;
  }

  public Button getButton() {
    return button;
  }

  public Animation getAnimation() {
    return animation;
  }

  public void removeAnimation() {
    animation = null;
    System.gc();
  }

  public void setActive(boolean a) {
    active = a;

    if (active) {
      if (animation == null) {
        animation = new Animation(animationLoader.loadAnimation(animationPath));
      }
      animation.play();
    } else {
      if (animation != null) {
        animation.play();
      }
    }
  }

  public boolean isActive() {
    return active;
  }
}
