class Button {
  private String label;
  private PVector pos;
  private PVector size;

  public Button(String label, float x, float y, float w, float h) {
    this.label = label;
    pos = new PVector(x, y);
    size = new PVector(w, h);
  }

  public void render(PGraphics offscreen) {
    offscreen.push();
    offscreen.textAlign(CENTER, CENTER);
    offscreen.textFont(font);
    offscreen.fill(255);
    offscreen.text(label, pos.x + size.x / 2, pos.y + size.y / 2);    
    offscreen.noFill();
    offscreen.strokeWeight(3);
    offscreen.stroke(255);
    //offscreen.rect(pos.x, pos.y, size.x, size.y);
    offscreen.pop();
  }
}
