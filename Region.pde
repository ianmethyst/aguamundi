/**
 * Area es la clase principal del proyecto. A partir de archivos JSON se pueden generar
 * diferentes áreas 
 */
class Region extends Surface {  
  /**
   *
   */

  PVector indicatorPosition;

  PImage standby;
  int standbyAlpha;
  boolean active;
  
  /**
   * 
   */
  private Animation indicator;
  private Animation transition;

  /**
   * Animación de transición para cuando se toca la Region
   */
  private Item[] items;

  /**
   * Referencia a instancia de clase Menu asociada. Al tocarse el área, se utilizara
   * el siguiente menú para dibujar los items.
   */
  private Menu menu;

  public Region(String name, int w, int h) {
    size = new PVector(w, h);

    path = sketchPath() + "/data/areas/" + name;

    // Load JSON
    JSONObject data = null;

    File [] files = new File(path).listFiles(new FilenameFilter() {
      @Override
        public boolean accept(File dir, String name) {
        return name.endsWith(".json");
      }
    }
    );

    for (File f : files) {
      data = loadJSONObject(f);
      break;
    }

    // Error checking

    if (data == null) {
      println("Error: json file not found in " + path);
      return;
    }

    String JSONname = data.getString("name");

    if (!name.equals(JSONname)) {
      println("Warning: name argument passed to Region constructor differs from name found in JSON key");
    }

    // Set variables

    id = data.getInt("id");
    this.name = JSONname;

    String menuName = data.getString("menu");
    for (Menu m : menues) {
      if (m.getName().equals(menuName)) {
        menu = m;
        break;
      }
    }

    // Items

    JSONArray JSONItems = data.getJSONArray("items");

    items = new Item[JSONItems.size()];

    for (int i = 0; i < JSONItems.size(); i++) {
      JSONObject currentItem = JSONItems.getJSONObject(i);

      String label = currentItem.getString("buttonLabel");
      String animationPath = currentItem.getString("animation");

      items[i] = new Item(i, menu, label, path + animationPath);
    }

    // Keystone setup

    surface = ks.createCornerPinSurface(int(size.x), int(size.y), 20);
    offscreen = createGraphics(int(size.x), int(size.y), P3D);

    // Load transition

    standby = loadImage(path + "/active.png");
    active = false;
    

    // Indicator
    JSONObject _indicatorOffset = data.getJSONObject("indicatorOffset");
    PVector off = new PVector(_indicatorOffset.getFloat("x"), _indicatorOffset.getFloat("y")); // From 0 to 1
    println(off.x, off.y);
    
    indicatorPosition = new PVector(offscreen.width / 2 + offscreen.width * off.x, offscreen.height / 2 + offscreen.width * off.y);
    indicator = regionIndicator;

    offscreen.beginDraw();
    offscreen.colorMode(HSB, 360, 100, 100);
    offscreen.endDraw();
  }

  public Item[] getItems() {
    return items;
  }

  public Menu getMenu() {
    return menu;
  }

  @Override
    public void update() {
      // Standby alpha

    for (Item i : items) {
      if (i.getAnimation() != null) {
        i.getAnimation().update();

        if (i.getAnimation().shouldRemove()) {
          i.removeAnimation();
        }
      }

      renderOffscreen();
    }
  }

  @Override
    protected void renderOffscreen() {
    offscreen.beginDraw();
    offscreen.clear();
    offscreen.imageMode(CENTER);
    //offscreen.tint(255, 180);
    offscreen.image(standby, offscreen.width / 2, offscreen.height / 2, offscreen.width, offscreen.height);

    if (activeRegion != this) {
      indicator.render(offscreen, indicatorPosition.x, indicatorPosition.y, offscreen.width / 2, offscreen.height / 2);
    } 

    for (Item i : items) {
      if (i.getAnimation() != null) {
        i.getAnimation().render(offscreen, offscreen.width / 2, offscreen.height / 2, offscreen.width, offscreen.height);
      }
    }

    offscreen.endDraw();
  }

  public void handleTouch(int plate) {
    if (plate == id) {
      for (Menu m : menues) {
        m.setCurrentRegion(null);
        activeMenu = null;
      }

      activeRegion = this;
      menu.setCurrentRegion(this);
      activeMenu = menu;
    }
  }

  public void handleMouseTouch() {
    if (surface.isMouseOver()) {
      handleTouch(id);
    }
  }

  public void close() {
    println("closing " + name);
    menu.setCurrentRegion(null);
    transition.play();
    transition.setDisappear(true);
    for (Item i : items) {
      i.setActive(false);
    }
  }

  @Override
    public void handleKeyPress() {
    if (surface.isMouseOver()) {
      if (key == '3') {
        println(name + ": position saved from " + path);
        savePosition(path);
      }

      if (key == '1') {
        println(name + ": position loaded from " + path);
        loadPosition(path);
      }
    }
  }

  /**
   * @param x position from 0 to 1.0
   * @param y position from 0 to 1.0
   */

  public void setIndicatorPosition(float x, float y) {
    indicatorPosition.set(map(x, 0, 1, 0, offscreen.width), map(x, 0, 1, 0, offscreen.height));
  }
}
