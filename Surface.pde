/**
 * La clase abstracta Surface representa una superficie que puede ser tocada.
 * Las clases Area y Menu heredan miembros y métodos de ésta. 
 */
abstract class Surface {
  /**
   * La id es usada para identificar el área de forma númerica. No debería haber 2 áreas
   * con la misma id, debido a que este número es utilizado para determinar que área física
   * fue tocada.
   */
  protected int id;

  /**
   * Variable utilizada para identificar por nombres a cada superficie. En la clase Menu tambien
   * sirve para asignarle un nombre al archivo de posición.
   */
  protected String name;

  /**
   * La variable path indica el subdirectorio de "/data/areas" en el que se encuentran
   * los archivos cada instancia de la clase
   */
  protected String path;

  /**
   * Tamaño de la superficie
   */
  protected PVector size;

  /**
   * instancia de CornerPinSurface utilizada para la corrección de matríz
   */
  protected CornerPinSurface surface;

  /**
   * Objeto PGraphics donde se dibujan los gráficos del área
   */
  protected PGraphics offscreen;

  /**
   * Este método dibuja el objeto offscreen en la pantalla.
   */
  public void display() {
    surface.render(offscreen);
  }

  /**
   * @returns ID de la superficie
   */
  public int getId() {
    return id;
  }

  public String getName() {
    return name;
  }

  /**
   * @returns Tamaño de la superficie
   */
  public PVector getSize() {
    return size;
  }

  /**
   * @returns La dirección del archivo de posición
   */
  public String getPath() {
    return path;
  }


  /**
   * @returns El objeto CornerPinSurface
   */
  public CornerPinSurface getSurface() {
    return surface;
  }

  /**
   * Guarda la posición y deformación de una superficie en un archivo json
   * @param path Directorio donde se guardará el archivo json
   */
  public void savePosition(String path) {
    JSONObject json = new JSONObject();
    JSONObject points = new JSONObject();

    json.setInt("res", surface.getRes());
    json.setFloat("x", surface.x);
    json.setFloat("y", surface.y);
    json.setInt("w", surface.getWidth());
    json.setInt("h", surface.getHeight());

    MeshPoint mesh[] = surface.getMesh();

    for (int i = 0; i < mesh.length; i++) {
      if (mesh[i].isControlPoint()) {
        JSONObject point = new JSONObject();
        point.setFloat("x", mesh[i].x);
        point.setFloat("y", mesh[i].y);
        point.setFloat("u", mesh[i].u);
        point.setFloat("v", mesh[i].v);

        points.setJSONObject(Integer.toString(i), point);
      }
    }

    json.setJSONObject("points", points);


    saveJSONObject(json, path + "/position");
  }


  /**
   * Carga la posición y deformación de una superficie desde un archivo json
   * @param path Directorio de donde se cargará el archivo json
   */
  public void loadPosition(String path) {
    try {
      JSONObject json = loadJSONObject(path + "/position");
      println(name + ": position loaded from " + path);

      surface.x = json.getFloat("x");
      surface.y = json.getFloat("y");

      // reload the mesh points
      JSONObject points = json.getJSONObject("points");

      String[] keys = (String[]) points.keys()
        .toArray(new String[points.size()]);

      for (String k : keys) {
        MeshPoint point = surface.getMesh()[int(k)];

        JSONObject current = points.getJSONObject(k);

        point.x = current.getFloat("x");
        point.y = current.getFloat("y");
        point.u = current.getFloat("u");
        point.v = current.getFloat("v");
        point.setControlPoint(true);
      }
      surface.calculateMesh();
    } 
    catch (Exception e) {
      println("ERROR: json file may not have been found");
      e.printStackTrace();
    }
  }

  /**
   * Update se utiliza para actualizar la superficie. Para hacerlo, llama a otros
   * métodos específicos de la clase.
   */
  abstract public void update();

  /**
   * Este método se encarga de dibujar todos los gráficos en el objeto offscreen.
   */
  abstract protected void renderOffscreen();

  /**
   * Este método controla que sucede cuando la superficie se toca.
   */
  //abstract public void handleTouch(int group, int plate);

  /**
   * Este método controla que sucede cuando se pulsa una tecla en el teclado.
   */
  abstract public void handleKeyPress();
}
